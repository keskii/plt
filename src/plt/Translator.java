package plt;

public class Translator {
	
	public static final String NIL = "nil";
	private String phrase;
	private String startConsonants;
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		
		if(startsWithVowel()) {
			
			if(isUpper()) {
				
				if(phrase.endsWith("Y")) {
					return phrase + "NAY";
				} else if(endsWithVowel()) {
					return phrase + "YAY";
				} else {
					return phrase + "AY";
				}
			} else {
				
				if(phrase.endsWith("y")) {
					return phrase + "nay";
				} else if(endsWithVowel()) {
					return phrase + "yay";
				} else {
					return phrase + "ay";
				}
			}
		}
		
		if(!startsWithVowel() && !phrase.equals("")) {
			if(isUpper()) {
				if(!endsWithPunctuation())
					return phrase.substring(countConsonants()) + startConsonants + "AY";
				else return phrase.substring(countConsonants(), phrase.length() - 1) + startConsonants + "AY" + phrase.substring(phrase.length() - 1);

			} else {
				
				if(Character.isUpperCase(phrase.charAt(0))) {
					phrase = phrase.toLowerCase();
					
					if(!endsWithPunctuation())
						phrase = phrase.substring(countConsonants()) + startConsonants + "ay";
					else
						phrase = phrase.substring(countConsonants(), phrase.length() - 1) + startConsonants + "ay" + phrase.substring(phrase.length() - 1);
					
					phrase = phrase.substring(0, 1).toUpperCase() + phrase.substring(1);
					return phrase;
				} else {
					
					if(!endsWithPunctuation())
						return phrase.substring(countConsonants()) + startConsonants + "ay";
					else return phrase.substring(countConsonants(), phrase.length() - 1) + startConsonants + "ay" + phrase.substring(phrase.length() - 1);
				}
			}
		}
		return NIL;
	}
	
	public String translateMoreWords() {
		StringBuilder finalPhraseStringBuilder = new StringBuilder();
		String[] moreWords = phrase.split("[ -]");
		Translator translatorMoreWords;
		int delimitatorsCounter = moreWords.length - 1;
		int delimitatorsAppended = 0;
		char[] delimitators = {' '};
		int j = 0;
		
		for(int i = 0; i < phrase.length(); i++) {
			
			if(phrase.charAt(i) == ' ' || phrase.charAt(i) == '-') {
				delimitators[j] = phrase.charAt(i);
				j++;
			}
		}
		
		for(int i = 0; i < moreWords.length; i++) {
			translatorMoreWords = new Translator(moreWords[i]);
			finalPhraseStringBuilder.append(translatorMoreWords.translate());
			
			if(delimitatorsCounter > delimitatorsAppended) {
				finalPhraseStringBuilder.append(delimitators[delimitatorsAppended]);
				delimitatorsAppended++;
			}
		}
		return finalPhraseStringBuilder.toString();
	}
	
	private boolean startsWithVowel() {
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u");
	}
	
	private boolean endsWithVowel() {
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");
	}
	
	private boolean isVowel(char c) {
		return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
	}
	
	private int countConsonants() {
		StringBuilder startConsonantsBuilder = new StringBuilder();
		int counter = 0;
		
		for(int i = 0; i < phrase.length(); i++) {
			
			if(!isVowel(phrase.charAt(i))) {
				startConsonantsBuilder.append(phrase.charAt(i));
				counter = i+1;
			} else break;
		}
		startConsonants = startConsonantsBuilder.toString();
		return counter;
	}
	
	private boolean endsWithPunctuation() {
		return phrase.endsWith(".") || phrase.endsWith(",") || phrase.endsWith(";") || phrase.endsWith(":") || phrase.endsWith("?") || phrase.endsWith("!") || phrase.endsWith("'") || phrase.endsWith("(") || phrase.endsWith(")");
	}
	
	private boolean isUpper() {
		boolean flag = true;
		
		for(int i = 0; i < phrase.length(); i++) {
			
			if(!Character.isUpperCase(phrase.charAt(i))) {
				flag = false;
			}
		}
		return flag;
	}

}

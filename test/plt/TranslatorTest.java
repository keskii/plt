package plt;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithY() {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingSingleConsonant() {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingMoreConsonant() {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWords() {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translateMoreWords());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsDashSeparated() {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellway-eingbay", translator.translateMoreWords());
	}
	
	@Test
	public void testTranslationPhraseWithMoreWordsContainingPunctuations() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway!", translator.translateMoreWords());
	}
	
	@Test
	public void testTranslationPhraseUpperCase() {
		String inputPhrase = "APPLE";
		Translator translator = new Translator(inputPhrase);
		assertEquals("APPLEAY", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseTitleCase() {
		String inputPhrase = "Hello";
		Translator translator = new Translator(inputPhrase);
		assertEquals("Ellohay", translator.translate());
	}
}
